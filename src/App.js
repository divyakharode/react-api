import React, { Component } from 'react';
    
    class App extends Component {
      state = {
        contacts: []
      }

      componentDidMount() {
        fetch('http://jsonplaceholder.typicode.com/users')
        .then(res => res.json())
        .then((data) => {
          this.setState({ contacts: data })
        })
        .catch(console.log)
      }

      render() {
        return (
          <div>
          <center><h1>Contact List</h1></center>
          {this.state.contacts.map((contact) => (
            <div class="card text-center">
              <div class="card-body">
                <h5 class="card-title">{contact.name}</h5>
                <h6 class="card-subtitle mb-2 text-muted">{contact.email}</h6>
                <p class="card-text">{contact.company.catchPhrase}</p>
              </div>
            </div>
          ))}
        </div>
        );
      }
    }
export default App;